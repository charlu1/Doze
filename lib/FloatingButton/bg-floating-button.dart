import 'package:flutter/material.dart';

class BgFloatingButton extends StatelessWidget {
  BgFloatingButton({@required this.color});
  final Color color;
  @override
  Widget build(BuildContext context) {
    return Container(
            decoration: new BoxDecoration(
              color: color, //new Color.fromRGBO(255, 0, 0, 0.0),
              // borderRadius: new BorderRadius.all(Radius.circular(40.0)),
              shape:BoxShape.circle,
              boxShadow: [
                BoxShadow(
                  color: Colors.black54,
                  blurRadius: 15.0,
                  spreadRadius: 3.0,
                  offset: Offset(10.0, 10.0),
                ),
              ]
            ),
          );
  }
}