import 'package:flutter/material.dart';
import 'package:vector_math/vector_math_64.dart' show Vector3;

class FloatingButtonTransition extends StatefulWidget {
  @override
  _FloatingButtonTransitionState createState() => _FloatingButtonTransitionState();
}

class _FloatingButtonTransitionState extends State<FloatingButtonTransition> {
  double _zoom = 1.0;
  
  void _animate() {
    setState(() => _zoom = 20);
  }
  Widget build(BuildContext context) {
  return GestureDetector(
      onTap: () {
        _animate();
      },
      child: Align(
        alignment: Alignment.bottomCenter,
        child: Transform.scale(
          scale: _zoom,
          child: Container(
            width: 80.0,
            height: 80.0,
            decoration: BoxDecoration(
              color: Colors.red, 
              borderRadius: BorderRadius.circular(40.0)),
          ),
        ),
      ),
    ); 
  }
}


