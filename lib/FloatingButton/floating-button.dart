import 'package:flutter/material.dart';
import 'package:doze/FloatingButton/bg-floating-button.dart';

class FloatingButton extends StatefulWidget {
  final GestureTapCallback onTap;
  final bool isActive;

  const FloatingButton ({ Key key, this.onTap, this.isActive}): super(key: key);

  @override
  _FloatingButtonState createState() => _FloatingButtonState();
}

class _FloatingButtonState extends State<FloatingButton> {
  bool _pressOn = false;

  @override
  Widget build(BuildContext context) {
    final _bg = FractionallySizedBox(
        widthFactor: _pressOn ? 0.9 : 1.0,
        heightFactor: _pressOn ? 0.9 : 1.0,
        child: BgFloatingButton(
      // edge: _pressOn ? widget.edge*0.9 : widget.edge,
      color:_pressOn ? Colors.orangeAccent : Colors.orange,
    )
    );

    //TODO: replace withimage
    final _plus = FractionallySizedBox(
        widthFactor: _pressOn ? 0.45 : 0.5,
        heightFactor: _pressOn ? 0.45 : 0.5,
        child: Opacity(
          opacity: widget.isActive ? 1.0 : 0.0, 
          child:Container(
            decoration: new BoxDecoration(
                color:_pressOn ? Colors.orange.shade100 : Colors.white,
                shape:BoxShape.circle,
              ),
          )
        )
     );

    return GestureDetector(
      onTap: () {
        print("button status : " + widget.isActive.toString());
        if(widget.isActive) {
          widget.onTap();
        }
      },
      onTapDown:(details) {
        if(widget.isActive)
          setState(() => _pressOn = true);
      },
      onTapUp:(details) {
        setState(() => _pressOn = false);
      },
      onTapCancel:() {
        setState(() => _pressOn = false);
      },
      child: Stack(
              // fit: StackFit.passthrough,
              alignment: Alignment.center,
              children: [_bg,_plus],
            )
    );
  }
}