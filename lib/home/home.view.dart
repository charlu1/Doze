import 'package:flutter/material.dart';
import '../FloatingButton/floating-button.dart';

class HomeView extends StatefulWidget {
  final GestureTapCallback openAddDoze;

  const HomeView({ Key key, this.openAddDoze}): super(key: key);

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  bool _isPlusActive = true;

  void _buttonTransition() {
    setState(() {
      _isPlusActive = false;
    });
    widget.openAddDoze();
    Future.delayed(const Duration(milliseconds: 500), () {
      setState(() {
        _isPlusActive = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final _contentView = Container(
      color: Colors.cyan,
    );
    final _btnAdd = Align(
      alignment: Alignment.bottomRight,
      child: AnimatedContainer(
        duration: Duration(milliseconds: 500),
        curve: Curves.easeOutBack,
        margin: EdgeInsets.fromLTRB(0.0, 0.0, 32.0, 48.0),
        width: 80.0,
        height: 80.0,
        child:FloatingButton(
          isActive : _isPlusActive,
          onTap: () {
            print("action fired");
            _buttonTransition();
          },
        )
      )
    );
    return Stack(
      alignment: Alignment.center,
      children: [_contentView,_btnAdd],
    );
  }
}