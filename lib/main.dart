import 'package:doze/root.view.dart';
import 'package:doze/scale-transition.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'FloatingButton/floating-button-transition.dart';
import 'home/home.view.dart';

void main() => runApp(MaterialApp(home: MyApp()));

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold( // use Scaffold also in order to provide material app widgets
      body: RootView(),
    );
  }
}


