import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';

class ScaleTransitionExample extends StatefulWidget {
  final GestureTapCallback transitionFinished;

  const ScaleTransitionExample({Key key, this.transitionFinished})
      : super(key: key);

  _ScaleTransitionExampleState createState() => _ScaleTransitionExampleState();
}

class _ScaleTransitionExampleState extends State<ScaleTransitionExample>
    with TickerProviderStateMixin {
  AnimationController _controller;
  Animation<double> _animation;

  initState() {
    super.initState();
    _controller = AnimationController(
        duration: const Duration(milliseconds: 500), vsync: this, value: 0.1);
    _animation = CurvedAnimation(parent: _controller, curve: Curves.easeInOut);
    Future.delayed(const Duration(milliseconds: 500), () {
      widget.transitionFinished();
    });
    _controller.forward();
  }

  @override
  dispose() {
    _controller.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomRight,
      child:Container(
        margin: EdgeInsets.fromLTRB(0.0, 0.0, 32.0, 48.0),
        color: Colors.transparent,
        child:ScaleTransition(
          scale: _animation,
          child: Transform.scale(
            scale: 25.0,
            child: Container(
              width: 80.0,
              height: 80.0,
              decoration: new BoxDecoration(
                color: Colors.orange,
                shape: BoxShape.circle,
              ),
            )
          )
        )
      )
    );
  }
}
