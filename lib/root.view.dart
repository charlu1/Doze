import 'package:doze/settings.view.dart';
import 'package:flutter/material.dart';
import 'FloatingButton/floating-button.dart';
import 'home/home.view.dart';
import 'scale-transition.dart';

class RootView extends StatefulWidget {
  @override
  _RootViewState createState() => _RootViewState();
}

class _RootViewState extends State<RootView> {
  List<Widget> _stacks = [];
  void _openAddDoze() {
    print("_openAddDoze");
    final _transitionToAddDoze =
        ScaleTransitionExample(transitionFinished: _transitionFinished);
    setState(() {
      _stacks.add(_transitionToAddDoze);
    });
  }

  void _transitionFinished() {
    print("_transitionFinished");
    final _settings = SettingsView();
    setState(() {
      _stacks.add(_settings);
    });
  }

  @override
  Widget build(BuildContext context) {
    final _home = HomeView(openAddDoze: _openAddDoze);
    if (_stacks.length == 0) _stacks = [_home];

    return Stack(
      // fit: StackFit.passthrough,
      alignment: Alignment.center,
      children: _stacks,
    );
  }
}
